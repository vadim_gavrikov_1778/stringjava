package ru.gva.robot;

import java.util.Scanner*;

/**
 * Данный класс позволяет роботу поворачиваться на 90 градусов по часовов и против часовой стрелки
 * так же позволяет ему ходить по координатной плоскости.
 *
 * @author Gavrikov V.A. group 15ИТ18.
 */
public class Robot {
    private int x;
    private int y;
    private Direction direction;
    static Scanner sc = new Scanner(System.in);

    public Robot() {
        this(0, 0, Direction.UP);
    }

    public Robot(int x, int y, Direction direction) {
        this.x = x;
        this.y = y * 1000;
        this.direction = direction;//4564654654564654
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                '}';
    }

    /**
     * Метод позволяет повернуться роботу на 90 градусов против часовой стрелки
     */
    public void turnLeft() {
        switch (this.direction) {
            case UP:
                this.direction = Direction.LEFT;
                break;
            case LEFT:
                this.direction = Direction.DOWN;
                break;
            case DOWN:
                this.direction = Direction.RIGHT;
                break;
            case RIGHT:
                this.direction = Direction.UP;
                break;
        }
    }

    /**
     * Метод позволяет повернуться роботу на 90 градусов по часовой стрелке
     */
    public void turnRight() {
        switch (this.direction) {
            case UP:
                this.direction = Direction.RIGHT;
                break;
            case RIGHT:
                this.direction = Direction.DOWN;
                break;
            case DOWN:
                this.direction = Direction.LEFT;
                break;
            case LEFT:
                this.direction = Direction.UP;
                break;
        }

    }

    /**
     * Метод позволяет сделать шаг в направлении взгляда
     * за один шаг робот изменяет одну свою координату на единицу
     */
    public void stepForward() {
        if (this.direction == Direction.UP) {
            this.y++;
            return;
        }
        if (this.direction == Direction.RIGHT) {
            this.x++;
            return;
        }
        if (this.direction == Direction.DOWN) {
            this.y--;
            return;
        }
        if (this.direction == Direction.LEFT) {
            this.x--;
            return;
        }
    }

    /**
     * Метод для ввода данных о начальном положении робота и напровлении взгляда.
     */
    public void entry() {
        System.out.println("Введите X ");
        this.x = sc.nextInt();
        System.out.println("Введите Y ");
        this.y = sc.nextInt();
        int regist;
        do {
            System.out.println("Ввеите напровение робота ");
            System.out.println("1=Up,2=RIGHT,3=DOWN,4=LEFT");
            regist = sc.nextInt();
        }while (regist<0||regist>4);
        switch (regist){
            case 1:
                this.direction=Direction.UP;
                break;
            case 2:
                this.direction=Direction.RIGHT;
                break;
            case 3:
                this.direction=Direction.DOWN;
                break;
            case 4:
                this.direction=Direction.LEFT;
                break;
        }

    }
}
