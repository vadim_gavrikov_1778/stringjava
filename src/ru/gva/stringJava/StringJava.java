package ru.gva.stringJava;

import java.io.*;
import java.util.regex.*;

/**
 * В данном классе реализованна программа для считывание строки из файла.
 * В строке осуществляется поиск с заменой коминтариев, незначащих пробелов, и имени класса.
 *
 * @author Garikov Vadim 15OIT18.
 */
public class StringJava {

    public static void main(String[] args) throws IOException {
        String s;
        Pattern pString = Pattern.compile(".*");
        Matcher m = pString.matcher("");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("/home/stillfak/Документы/project1/StringJava/Robot.java")));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File("/home/stillfak/Документы/project1/StringJava/Robot2.java")))) {
            while ((s = bufferedReader.readLine()) != null) {
                m.reset(s);
                while (m.find()) {
                    s = m.group().replaceAll("^\\s*\\/?\\*[^\\/]*[[*]-[/]]?$", "");
                    s = s.replaceAll("\\/\\/.*","");
                    s = s.replaceAll("\\s+\\s+","");
                    s = s.replaceAll("Robot","Robot2");
                    bufferedWriter.write(s);
                }
            }
        }
    }
}
